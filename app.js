const express = require("express");

const app = express();
// const ejs = require("ejs");

const mongoose = require("mongoose");

const fs = require("fs");
const { response } = require("express");
const port = 3000;
mongoose.connect("mongodb://localhost/rsvp", {
  useNewUrlParser: true,
});

const db = mongoose.connection;

// app.set("view engine", "ejs");

app.set("view engine", "pug");

app.set("views", "./views");

app.use(express.static("./public"));
app.use(express.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  res.render("index", {});
});

app.post("/reply", async (req, res) => {
  const response = new responseModel({
    name: req.body.name,
    email: req.body.email,
    attending: req.body.attending,
    guest: req.body.guests,
  });
  await response.save();
  res.render("index2");
});

app.get("/guestlists", (req, res) => {
  let attending = [];
  let notattending = [];
  responseModel.find({}, (err, reser) => {
    console.log(reser);
    reser.forEach((rez) => {
      if (rez.attending) {
        attending.push(JSON.stringify(rez));
      } else {
        notattending.push(JSON.stringify(rez));
      }
    });
  });
  console.log(attending);
  res.render("guestlist", { attending: attending, notattending: notattending });
});

// app.get("/", (req, res) => res.send("index"));

const ResponseSchema = mongoose.Schema({
  name: String,
  email: String,
  attending: Boolean,
  guest: Number,
});

const responseModel = mongoose.model("Response", ResponseSchema);

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));
